const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mysqlConnection = require('../database.js');

const router = express.Router();

router.post('/login', (req, res) => {
    const { username, password } = req.body;

    mysqlConnection.query('SELECT * FROM user WHERE username = ?', [username], (err, rows, fields) => {
        if (err) {
            return console.log(err);
        }

        if (!rows[0]) {
            return res.status(400).json({
                ok: false,
                err: {
                    mensaje: 'Usuario o contraseña incorrectos'
                }
            })
        }
        if (!bcrypt.compareSync(password, rows[0].password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    mensaje: 'Usuario o contraseña incorrectos'
                }
            })
        }
        delete rows[0].password;
        delete rows[0].create_time;
        let token = jwt.sign({
            usuario: rows[0]
        }, process.env.SEMILLA, { expiresIn: process.env.CADUCIDAD })
        res.json({
            ok: true,
            usuario: rows[0],
            token
        })
    });
})

module.exports = router;