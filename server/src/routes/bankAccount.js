const express = require('express');
const mysqlConnection = require('../database.js');
const { verificador } = require('../../middlewares/autenticacion');


const router = express.Router();

router.get('/bankAccount/', verificador, (req, res) => {

    mysqlConnection.query('SELECT * FROM bankAccount', (err, rows, fields) => {
        if (!err) 
            res.json(rows);
        else 
            res.json(err);
    }); 
});

router.get('/bankAccount/:idbankAccount', verificador, (req, res) => {
    const { idbankAccount } = req.params;
    mysqlConnection.query('SELECT * FROM bankAccount WHERE bankAccount.idbankAccount = ?', [idbankAccount], (err, rows, fields) => {
        if (!err) 
            res.json(rows[0]);
        else 
            res.json(err);
    });
});

router.delete('/bankAccount/:idbankAccount', verificador, (req, res) => {
    const { idbankAccount } = req.params;
    mysqlConnection.query('DELETE FROM bankAccount WHERE idbankAccount = ?', [idbankAccount], (err, rows, fields) => {
        if (!err) 
            res.json({ status: 'delted Bank Account' });
        else 
            res.json(err);
    });
});

router.post('/bankAccount', verificador, (req, res) => {
    const { number, bank, artist_user_username } = req.body;
    const query = `
        INSERT INTO bankAccount (number, bank, artist_user_username)
        VALUES (?, ?, ?);
    `;
    mysqlConnection.query(query, [number, bank, artist_user_username], (err, rows, fields) => {
        if (!err) 
            res.json({ status: 'created Bank Account!' });
        else 
            res.json(err);
    });

});

router.put('/bankAccount/:idbankAccount', verificador, (req, res) => {
    const { number, bank, artist_user_username } = req.body;
    const { idbankAccount } = req.params;
    const query = `
    UPDATE bankAccount
    SET
    number = ?,
    bank = ?,
    artist_user_username = ?,
    WHERE idbankAccount = ?;
  `;
    mysqlConnection.query(query, [number, bank, artist_user_username], (err, rows, fields) => {
        if (!err) 
            res.json({ status: 'updated Bank Account' });
        else 
            res.json(err);
    });
});

module.exports = router;