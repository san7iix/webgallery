const router = require('express').Router();
const paypalConfig = require('../configPaypal.json');
const { verificador } = require('../../middlewares/autenticacion');
var paypal = require('paypal-rest-sdk');


// SETUP PAYPAL CONFIG
paypal.configure(paypalConfig);

// CREATE PAYMENT JSON
const createPaymentJson = ({ shopping_cart, valor, description }) => ({
    "intent": "sale",
    "payer": { "payment_method": "paypal" },
    "redirect_urls": { "return_url": "http://localhost:3001/success", "cancel_url": "http://localhost:3001/cancel" },
    "transactions": [{
        "item_list": { "items": shopping_cart },
        "amount": valor,
        "description": description
    }]
});

const executePaymentJson = ({ payerId, valor }) => ({
    "payer_id": payerId,
    "transactions": [{
        "amount": valor
    }]
});



// ROUTES - PAGE
router.get('/', verificador ,(req,res) => res.render('index', { artwork }) );

// ROUTES WITH PAYPAL
let globalArtworkSelected;
router.post('/buy',  verificador ,(req,res) => {
    const artworkId = req.query.id;
    const artwork = 1; // query to artwork!

    if(!artwork.id) return res.render('index', { artworks });

    const shopping_cart = [{
        "name": artwork.title,
        "sku": artwork.idartwork,
        "price": artwork.price.toFixed(2),
        "currency": "USD",
        "quantity": 1
    }];
    const valor = { "currency": "USD", "total": artwork.price.toFixed(2) };
    const description = artwork.description;
    const create_payment_json = createPaymentJson({ shopping_cart, valor, description });

    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) throw error;
        else {
            globalArtworkSelected = artwork; // SEM DB
            payment.links.forEach((link) => {
                if(link.rel === 'approval_url') return res.redirect(link.href);
            });
        }
    });

});

// ROUTER res Success
router.get('/success', verificador ,(req,res) => {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
    const valor = {
        "currency": "USD",
        "total": globalArtworkSelected.price.toFixed(2) // SEM DB
    };
    const execute_payment_json = executePaymentJson({ payerId, valor });

    paypal.payment.execute(paymentId, execute_payment_json, (error, payment) => {
        if(error){
            console.log(error.response);
            throw error; // views err
        } else {
            console.log('Get Payment Response');
            console.log(JSON.stringify(payment));
                 // views sucess
            res.render('success', { payment });
        }
    });
    res.send('Success'); //views sucess
});

// ROUTER res cancel
router.get('/cancel', verificador ,(req,res) => {
    res.send('Cancelled'); // views cancel
});

module.exports = router;



  