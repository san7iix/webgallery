const express = require('express');
const mysqlConnection = require('../database.js');
const { verificador } = require('../../middlewares/autenticacion')


const router = express.Router();

router.get('/customer', verificador ,(req, res) => {
    mysqlConnection.query('SELECT * FROM customer INNER JOIN user ON user.username=customer.user_username', (err, rows, fields) => {
        if (!err) 
            res.json(rows);
        else 
            res.json(err);        
    });
});

router.get('/customer/:username', verificador ,(req, res) => {
    const { username } = req.params;
    mysqlConnection.query('SELECT * FROM customer INNER JOIN user ON user.username = customer.user_username WHERE customer.user_username = ?', [username], (err, rows, fields) => {
        if (!err) 
            res.json(rows[0]);
        else 
            res.json(err);
    });
});

router.delete('/customer/:username', verificador ,(req, res) => {
    const { username } = req.params;
    mysqlConnection.query('DELETE FROM customer WHERE user_username = ?', [username], (err, rows, fields) => {
        if (!err) 
            res.json({ status: 'deleted customer!' });
        else 
            res.json(err);
    });
});

router.post('/customer', verificador ,(req, res) => {
    const { user_username, doc } = req.body;
    const query = `
        INSERT INTO customer (user_username, doc)
        VALUES (?, ?);
    `;
    mysqlConnection.query(query, [user_username,doc], (err, rows, fields) => {
        if (!err) 
            res.json({ status: 'created customer!' });
        else 
            res.json(err);
    });

});

router.put('/customer/:username',verificador,(req, res) => {
    const { doc } = req.body;
    const { username } = req.params;
    const query = 
        `
        UPDATE customer
        SET
        doc = ?,
        WHERE user_username = ?;
        `;

   mysqlConnection.query(query, [doc,username], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'updated customer' });
        } else {
            console.log(err);
        }
   });
});

module.exports = router;