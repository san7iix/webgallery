const express = require('express');
const bcrypt = require('bcryptjs');
const mysqlConnection = require('../database.js');
const { verificador } = require('../../middlewares/autenticacion')

const router = express.Router();

router.get('/usuarios',(req, res) => {
    mysqlConnection.query('SELECT * FROM user', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

router.get('/usuarios/:username',  (req, res) => {
    const { username } = req.params;
    mysqlConnection.query('SELECT username,name,surname,email,tel FROM user WHERE username = ?', [username], (err, rows, fields) => {
        if (!err) {
            res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
});

router.delete('/eliminarU/:username',  (req, res) => {
    const { username } = req.params;
    mysqlConnection.query('DELETE FROM user WHERE username = ?', [username], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'Usuario eliminado' });
        } else {
            console.log(err);
        }
    });
});

router.post('/', verificador, (req, res) => {
    const { username, email, password, name, surname, tel } = req.body;
    const query = `
  INSERT INTO user (username, email, password, name, surname, tel)
    VALUES (?, ?, ?, ?, ?, ?);
  `;
    mysqlConnection.query(query, [username, email, bcrypt.hashSync(password, 10), name, surname, tel], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'usuario creado' });
        } else {
            console.log(err);
        }
    });

});

router.post('/registro', (req, res) => {
    const { username, email, password, name, surname, tel } = req.body;
    const query = `
  INSERT INTO user (username, email, password, name, surname, tel)
    VALUES (?, ?, ?, ?, ?, ?);
  `;
    mysqlConnection.query(query, [username, email, bcrypt.hashSync(password, 10), name, surname, tel], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'ok' });
        } else {
            console.log(err);
        }
    });

});

router.put('/editarU', (req, res) => {
    const { username,email, password, name, surname, tel } = req.body;

    console.log(req.body)

    const query2 = `
    UPDATE user
    SET
    email = ?,
    password = ?,
    name = ?,
    surname = ?,
    tel = ?
    WHERE username = ?;
  `;
    mysqlConnection.query(query2, [email, bcrypt.hashSync(password, 10), name, surname, tel, username], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'Usuario actualizado' });
        } else {
            console.log(err);
        }
    });
});

module.exports = router;