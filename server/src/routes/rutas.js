const express = require('express');
const app = express();

app.use(require('./usuarios'));
app.use(require('./login'));
app.use(require('./artistas'));
app.use(require('./bankAccount'));
app.use(require('./customers'));
app.use(require('./shippingDetails'));
app.use(require('./payment'));

module.exports = app;