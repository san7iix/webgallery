const express = require('express');
const mysqlConnection = require('../database.js');
const { verificador } = require('../../middlewares/autenticacion');


const router = express.Router();


router.get('/shipping', verificador , (req,res) =>{

    mysqlConnection.query('SELECT * FROM shippingDetails',(err,rows,fields) => {

        if(!err)
           res.json(rows);
        else
           res.json(err);
    });

});


router.get('/shipping/:order_code', verificador , (req,res) =>{

    const {order_code} = req.params;

    mysqlConnection.query('SELECT * FROM shippingDetails WHERE order_code = ?',[order_code],(err,rows,fields) => {

        if(!err)
           res.json(rows[0]);
        else
           res.json(err);
    });

});

router.post('/shipping', verificador ,(req, res) => {


    const {order_code, name, surname,doc,department,city,address,phone,delivery} = req.body;
    const query = `
        INSERT INTO shippingDetails (order_code, name, surname,doc,department,city,address,phone,delivery)
        VALUES (?,?,?,?,?,?,?,?,?);
    `;

    mysqlConnection.query(query, [order_code, name, surname,doc,department,city,address,phone,delivery], (err,rows,fields) =>{

        if(!err)
            res.json({status:"Created shippingDetails!"});
        else
            res.json(err);
    });


});

router.put('/shipping/:order_code', verificador ,(req, res) => {

    const { name, surname,doc,department,city,address,phone,delivery} = req.body;
    const { order_code } = req.params;

    const queryup = `
    UPDATE shippingDetails
    SET 
    name = ?, 
    surname = ?,
    doc = ?,
    department = ?,
    city = ?,
    address = ?,
    phone = ?,
    delivery = ?
    WHERE order_code = ?;
    `;

    mysqlConnection.query(queryup, [order_code,name, surname,doc,department,city,address,phone,delivery], (err,rows, fields)=>{

        if(!err)
            res.json({status:'updated shippingDetails'});
        else
            res.json(err);

    });
});

module.exports = router;
