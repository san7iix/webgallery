const express = require('express');
const bcrypt = require('bcryptjs');
const mysqlConnection = require('../database.js');
const { verificador } = require('../../middlewares/autenticacion')

const router = express.Router();

router.get('/artista', verificador, (req, res) => {
    mysqlConnection.query('SELECT * FROM artist INNER JOIN user ON user.username=artist.user_username', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

router.get('/artista/:username', verificador, (req, res) => {
    const { username } = req.params;
    mysqlConnection.query('SELECT * FROM artist INNER JOIN user ON user.username=artist.user_username WHERE artist.user_username = ?', [username], (err, rows, fields) => {
        if (!err) {
            res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
});

router.delete('/artista/:username', verificador, (req, res) => {
    const { username } = req.params;
    mysqlConnection.query('DELETE FROM artist WHERE user_username = ?', [username], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'usuario eliminado' });
        } else {
            console.log(err);
        }
    });
});

router.post('/artista', verificador, (req, res) => {
    const { user_username, biography, tel } = req.body;
    const query = `
  INSERT INTO artist (user_username, biography, tel)
    VALUES (?, ?, ?);
  `;
    mysqlConnection.query(query, [user_username, biography, tel], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'usuario creado' });
        } else {
            console.log(err);
        }
    });

});

router.put('/artista/:username', verificador, (req, res) => {
    const { biography, tel } = req.body;
    const { username } = req.params;
    const query2 = `
    UPDATE artist
    SET
    biography = ?,
    tel = ?
    WHERE user_username = ?;
  `;
    mysqlConnection.query(query2, [biography, tel, username], (err, rows, fields) => {
        if (!err) {
            res.json({ status: 'usuario actualizado' });
        } else {
            console.log(err);
        }
    });
});

module.exports = router;