const express = require('express');
const app = express();
var cors = require('cors');
var bodyParser = require('body-parser');

app.use(cors());


//app.set('port', process.env.PORT || 3001);
app.use(require('../config/config'));

app.use(express.json());

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use(require('./routes/rutas'));

app.listen(process.env.PORT, () => {
    console.log(`Server on port ${process.env.PORT}`);
});