-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema webgallerydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema webgallerydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `webgallerydb` DEFAULT CHARACTER SET utf8 ;
USE `webgallerydb` ;

-- -----------------------------------------------------
-- Table `webgallerydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`user` (
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `tel` VARCHAR(45) NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));


-- -----------------------------------------------------
-- Table `webgallerydb`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`category` (
  `category_id` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`category_id`));


-- -----------------------------------------------------
-- Table `webgallerydb`.`artist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`artist` (
  `user_username` VARCHAR(255) NOT NULL,
  `biography` VARCHAR(255) NULL,
  `tel` VARCHAR(45) NULL,
  PRIMARY KEY (`user_username`),
  INDEX `fk_artist_user1_idx` (`user_username` ASC),
  CONSTRAINT `fk_artist_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`customer` (
  `user_username` VARCHAR(255) NOT NULL,
  `doc` VARCHAR(45) NULL,
  PRIMARY KEY (`user_username`),
  INDEX `fk_customer_user1_idx` (`user_username` ASC),
  CONSTRAINT `fk_customer_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`artwork`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`artwork` (
  `idartwork` INT NOT NULL,
  `title` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `price` INT NULL,
  `stock` INT NULL,
  `artist_user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`idartwork`),
  INDEX `fk_artwork_artist1_idx` (`artist_user_username` ASC),
  CONSTRAINT `fk_artwork_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`review` (
  `idreview` INT NOT NULL,
  `qualification` INT NULL,
  `text` VARCHAR(45) NULL,
  `create_time` DATETIME NULL,
  `artwork_idartwork` INT NOT NULL,
  `user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`idreview`),
  INDEX `fk_review_artwork1_idx` (`artwork_idartwork` ASC) ,
  INDEX `fk_review_user1_idx` (`user_username` ASC),
  CONSTRAINT `fk_review_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`address` (
  `idaddress` INT NOT NULL,
  `department` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `postcode` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `customer_user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`idaddress`),
  INDEX `fk_address_customer1_idx` (`customer_user_username` ASC),
  CONSTRAINT `fk_address_customer1`
    FOREIGN KEY (`customer_user_username`)
    REFERENCES `webgallerydb`.`customer` (`user_username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`picture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`picture` (
  `image` VARCHAR(45) NULL,
  `artwork_idartwork` INT NOT NULL,
  INDEX `fk_picture_artwork1_idx` (`artwork_idartwork` ASC),
  CONSTRAINT `fk_picture_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`feature`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`feature` (
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  `artwork_idartwork` INT NOT NULL,
  PRIMARY KEY (`title`),
  INDEX `fk_feature_artwork1_idx` (`artwork_idartwork` ASC),
  CONSTRAINT `fk_feature_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`order` (
  `code` VARCHAR(10) NOT NULL,
  `total` VARCHAR(45) NULL,
  `customer_user_username` VARCHAR(16) NOT NULL,
  `artist_user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`code`),
  INDEX `fk_order_customer1_idx` (`customer_user_username` ASC),
  INDEX `fk_order_artist1_idx` (`artist_user_username` ASC),
  CONSTRAINT `fk_order_customer1`
    FOREIGN KEY (`customer_user_username`)
    REFERENCES `webgallerydb`.`customer` (`user_username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`shippingDetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`shippingDetails` (
  `order_code` VARCHAR(10) NOT NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `doc` VARCHAR(45) NULL,
  `department` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `delivery` VARCHAR(45) NULL,
  PRIMARY KEY (`order_code`),
  CONSTRAINT `fk_shippingDetails_order`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`billingDetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`billingDetails` (
  `order_code` VARCHAR(10) NOT NULL,
  `status` INT NULL,
  `create_at` TIMESTAMP NULL,
  `paid_at` TIMESTAMP NULL,
  PRIMARY KEY (`order_code`),
  CONSTRAINT `fk_billingDetails_order1`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`bankAccount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`bankAccount` (
  `idbankAccount` INT NOT NULL,
  `number` VARCHAR(45) NULL,
  `bank` VARCHAR(45) NULL,
  `artist_user_username` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`idbankAccount`),
  INDEX `fk_bankAccount_artist1_idx` (`artist_user_username` ASC),
  CONSTRAINT `fk_bankAccount_artist1`
    FOREIGN KEY (`artist_user_username`)
    REFERENCES `webgallerydb`.`artist` (`user_username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`orderDetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`orderDetails` (
  `artwork_idartwork` INT NOT NULL,
  `order_code` VARCHAR(10) NOT NULL,
  `quantity` INT NULL,
  PRIMARY KEY (`artwork_idartwork`, `order_code`),
  INDEX `fk_artwork_has_order_order1_idx` (`order_code` ASC),
  INDEX `fk_artwork_has_order_artwork1_idx` (`artwork_idartwork` ASC),
  CONSTRAINT `fk_artwork_has_order_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_artwork_has_order_order1`
    FOREIGN KEY (`order_code`)
    REFERENCES `webgallerydb`.`order` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`admin` (
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `webgallerydb`.`category_has_artwork`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`category_has_artwork` (
  `category_category_id` INT NOT NULL,
  `artwork_idartwork` INT NOT NULL,
  PRIMARY KEY (`category_category_id`, `artwork_idartwork`),
  INDEX `fk_category_has_artwork_artwork1_idx` (`artwork_idartwork` ASC),
  INDEX `fk_category_has_artwork_category1_idx` (`category_category_id` ASC),
  CONSTRAINT `fk_category_has_artwork_category1`
    FOREIGN KEY (`category_category_id`)
    REFERENCES `webgallerydb`.`category` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_category_has_artwork_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `webgallerydb`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webgallerydb`.`comment` (
  `idcomment` INT NOT NULL,
  `text` VARCHAR(255) NULL,
  `create_time` TIMESTAMP NULL,
  `user_username` VARCHAR(16) NOT NULL,
  `artwork_idartwork` INT NOT NULL,
  PRIMARY KEY (`idcomment`),
  INDEX `fk_comment_user1_idx` (`user_username` ASC),
  INDEX `fk_comment_artwork1_idx` (`artwork_idartwork` ASC),
  CONSTRAINT `fk_comment_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `webgallerydb`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_artwork1`
    FOREIGN KEY (`artwork_idartwork`)
    REFERENCES `webgallerydb`.`artwork` (`idartwork`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
