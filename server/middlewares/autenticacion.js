const jwt = require('jsonwebtoken');


//=======================
// verificacion del token
//=======================

let verificador = (req, res, next) => {
    let token = req.get('token');
    console.log(token)
    jwt.verify(token, process.env.SEMILLA, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            })
        }
        req.user = decoded.usuario
        next();
    })
};

module.exports = { verificador };