const express = require('express');
const app = express();

//=======================
// puerto
//=======================
//app.set('port', process.env.PORT || 3000);
process.env.PORT = process.env.PORT || 3000

//=======================
// vencimiento del token
//=======================
// 60 segundos 60 minutos 24 horas
process.env.CADUCIDAD = 60 * 60 * 24 * 30;


//=======================
// semilla de autenticacion
//=======================
process.env.SEMILLA = 'semilla-de-autenticacion';




module.exports = app;