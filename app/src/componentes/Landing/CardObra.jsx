import React from 'react';
import './css/cardObra.css';


class CardObra extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div className="card">
          <h3>{this.props.titulo}</h3>
          <img src={this.props.imagen} alt="img"></img>
          <p>
            {this.props.desc}
          </p>
          <div className="boton-card">
              <button>Ver más...</button>
          </div>
        </div>
    );
  }
}

export default CardObra;