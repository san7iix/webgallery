import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faUserCircle, faDoorOpen } from '@fortawesome/free-solid-svg-icons'
import logo from './logo.png'
import AuthService from '../../api_interact/auth'


class Header extends React.Component {

  constructor(props){
    super(props);
    this.state = {isLoggedIn: localStorage.getItem("user_uaeh_token")}
    this.logout = this.logout.bind(this)
  }

  logout(){
    AuthService.logout()
    window.location.replace('')
  }
  render() {
    const isLoggedIn = this.state.isLoggedIn;
    let logout, vender,perfil,ingresar,admin,registro;
    if(isLoggedIn){
      logout = <a href="/"><FontAwesomeIcon onClick={this.logout} icon={faDoorOpen} /></a>
      vender = <a href="/vender">Vender</a>
      perfil = <a href="/profile"><FontAwesomeIcon icon={faUserCircle} /></a>
      admin = <a href="/admin">Panel administrativo</a>
    }
    if(!isLoggedIn){
      registro = <a href="/registro">Registrarse</a>
      ingresar = <a href="/login">Ingresar</a>
    }
    return (
      <header>
          <div className="izq_text">
            <img id="logo_header" src={logo}/>
            <input type="text" placeholder="Buscar..." className="buscar"></input>
          </div>
          <div className="links_der">
            <div className="links">
                {admin}
                {ingresar}
                {registro}
                <a href="#">Categorías</a>
                <a href="#">Ofertas</a>
                {vender}
            </div>
            <div className="links_iconos">
                <a href="/"><FontAwesomeIcon icon={faHome}></FontAwesomeIcon></a>
                {perfil}
                {logout}
                
            </div>
          </div>
      </header>
    );
  }
}

export default Header;