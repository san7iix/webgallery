import React, {Fragment} from 'react';

class SuccessPayment extends React.Component{

    render() {
        return (
            <div class="card_principal">
                <h1>Su pago fue realizado!</h1>
                <br/>
                <p>Su compra <strong>(ID/* payment.id */)</strong>  tuvo un pago satisfactorio.</p>
                <br/>
                <p>Para mas detalles de su compra consultar <a href="https://www.sandbox.paypal.com/co/signin">Paypal.</a></p>  
                <br/>
                <h3>Gracias por preferirnos!</h3>
            </div>
        );
    }

}


export default SuccessPayment;
