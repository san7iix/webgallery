import React, {Fragment} from 'react';
import '../Vender/css/vender.css'

class Vender extends React.Component {
  render() {
    return (
        <Fragment>
          <div className="card_principal vender">
          <h1>Vender</h1>
            <div className="imagen_vender">
              <img alt="img_obra" src="https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350"></img>
            </div>
            <div className="grupo_form">
              <input type="text" className="main_input" placeholder="Nombre de la obra"></input>
            </div>
            <div className="grupo_form">              
              <textarea type="text" className="main_input text_area" placeholder="Descripción" resize="none"/>
            </div>
            <div className="grupo_form">              
            </div>
            <div className="grupo_form">              
              <input type="text" className="main_input" placeholder="username artista"></input>
            </div>
            <div className="grupo_form">              
              <input type="text" className="main_input" placeholder="Stock"></input>
            </div>
            <div className="grupo_form">              
              <input type="text" className="main_input" placeholder="Precio ($COP)"></input>
            </div>
            <div className="boton_form_venta">
              <button>Publicar</button>
            </div>
          </div>            
        </Fragment>
    );
  }
}

export default Vender;