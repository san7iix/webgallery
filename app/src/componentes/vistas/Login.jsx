import React, {Fragment} from 'react';
import '../Login/css/Login.css'
import AuthService from '../../api_interact/auth'

class Login extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      username : '',
      password : ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.loginAuth = this.loginAuth.bind(this)
  }


  loginAuth (e){
    e.preventDefault()
    const user = this.state.username;
    const password = this.state.password
    AuthService.login(user, password)
      .then(r=>{
        if(r.ok){
          window.location.replace('/')
        }else{
          alert(r.err.mensaje)
        }
      })
      .catch(e=>{
        console.log(e)
      })

  }

  handleChange(e){
    const { name, value } = e.target
    this.setState({
      [name]:value
    })
  }

  render() {
    return (
        <div class="card_principal" id="login_form" onSubmit={this.loginAuth}>
          <form className="" action="#">
            <h1>Login</h1>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="usuario" name="username" placeholder="Usuario"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="password" className="main_input" id="password" name="password" placeholder="Contraseña"></input>
            </div>
            <div className="boton_form_venta">
              <button onClick={this.handleSubmit}>Ingresar</button>
            </div>
          </form>
        </div>
    );
  }
}

export default Login;