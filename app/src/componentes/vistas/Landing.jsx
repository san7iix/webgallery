import React, {Fragment} from 'react';
import Aviso from '../Landing/aviso'
import Recientes from '../Landing/Recientes'


class Landing extends React.Component {
  render() {
    return (
        <Fragment>        
            <Aviso/>
            <Recientes/>
        </Fragment>
    );
  }
}

export default Landing;