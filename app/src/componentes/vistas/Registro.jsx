import React, {Fragment} from 'react';
import '../Login/css/Login.css'
import AuthService from '../../api_interact/auth'

class Registro extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      username : '',
      password : '',
      surname : '',
      email : '',
      name : '',
      tel : ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.registroAuth = this.registroAuth.bind(this)
  }


  registroAuth (e){
    e.preventDefault()
    const { username, password, name, surname, tel, email} = this.state
    AuthService.registro(username, email, password,name,surname,tel)
      .then(r=>{
        if(r.status == 'ok'){            
            window.location.replace('/')
        }else{
          alert(r.err.mensaje)
        }
      })
      .catch(e=>{
        console.log(e)
      })

  }

  handleChange(e){
    const { name, value } = e.target
    this.setState({
      [name]:value
    })
  }

  render() {
    return (
        <div class="card_principal" onSubmit={this.registroAuth}>
          <form className="" action="#">
            <h1>Login</h1>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="usuario" name="username" placeholder="Usuario"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="password" className="main_input" id="password" name="password" placeholder="Contraseña"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="name" name="name" placeholder="Nombre"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="surname" name="surname" placeholder="Apellido"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="email" className="main_input" id="email" name="email" placeholder="Email"></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="tel" name="tel" placeholder="Teléfono"></input>
            </div>
            <div className="boton_form_venta">
              <button onClick={this.handleSubmit}>Registrarme</button>
            </div>
          </form>
        </div>
    );
  }
}

export default Registro;