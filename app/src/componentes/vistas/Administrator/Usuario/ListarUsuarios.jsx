import React, {Fragment} from 'react';
import UsuariosService from '../../../../api_interact/adminUsuarios'
import '../Usuario/css/Usuarios.css'


class ListarUsuarios extends React.Component {

  constructor(props){
    super(props)

    this.state = {
      usuarios: []
    }
    this.obtenerUsuarios = this.obtenerUsuarios.bind(this)
    this.eliminarUsuario = this.eliminarUsuario.bind(this)
  }

  obtenerUsuarios(){
    UsuariosService.getUsuarios()
    .then(res=>{
      this.setState({usuarios : res})
    })
  }

  eliminarUsuario(usuario){
    UsuariosService.eliminarUsuario(usuario)
    .then((res)=>{
      alert(res.status)
    })
    this.obtenerUsuarios()
  }

  componentDidMount(){
    this.obtenerUsuarios()
  }

  render() {
    return (
        <Fragment>
            <div className="card_principal">
              <h1>Usuarios registrados</h1>
              <button onClick={this.obtenerUsuarios}>Actualizar</button>
              <table>
                <thead>
                  <tr>
                    <td>Username</td>
                    <td>Nombre</td>
                    <td>Email</td>
                    <td>surname</td>
                    <td>tel</td>
                    <td>Acciones</td>
                  </tr>
                </thead>
                <tbody>
                {
                  this.state.usuarios.map(usuario=>{
                    return(
                      <tr key={usuario.username}>
                        <td>{usuario.username}</td>
                        <td>{usuario.name}</td>
                        <td>{usuario.email}</td>
                        <td>{usuario.surname}</td>
                        <td>{usuario.tel}</td>
                        <td>
                          <button onClick={()=>this.eliminarUsuario(usuario.username)}>Eliminar</button>
                          <button onClick={()=>
                            this.props.history.push('./editarU/' + usuario.username, { username: usuario.username })
                          }>Editar</button>
                        </td>
                      </tr>
                    )
                  })
                }                  
                </tbody>
              </table>
            </div>        
        </Fragment>
    );
  }
}

export default ListarUsuarios;