import React, {Fragment} from 'react'
import UsuariosService from '../../../../api_interact/adminUsuarios'
import '../Usuario/css/Usuarios.css'


class EditarUsuarios extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username : this.props.match.params.username,
      password : '',
      surname : '',
      email : '',
      name : '',
      tel : ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.editarUsuario = this.editarUsuario.bind(this)
  }

  handleChange(e){
    const { name, value } = e.target
    this.setState({
      [name]:value
    })
  }

  obtenerUsuario(usuario){
    UsuariosService.obtenerUsuario(usuario)
    .then((data)=>{
      this.setState({
        surname: data.surname,
        email: data.email,
        name: data.name,
        tel: data.tel,
      })
    })
  }

  editarUsuario(e){
    e.preventDefault()
    UsuariosService.editarUsuario(this.state)
    .then(res=>{
      alert(res.status)
      this.props.history.push('/admin/listarU')
    })
  }

  componentDidMount(){
    this.obtenerUsuario(this.state.username)
  }

  render() {
    return(
        <div className="card_principal" onSubmit={this.editarUsuario}>
          <form className="" action="#">
            <h1>Login</h1>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="usuario" name="username" placeholder="Usuario" value={this.state.username}></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="password" className="main_input" id="password" name="password" placeholder="Contraseña" required></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="name" name="name" placeholder="Nombre" value={this.state.name}></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="surname" name="surname" placeholder="Apellido" value={this.state.surname}></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="email" className="main_input" id="email" name="email" placeholder="Email" value={this.state.email}></input>
            </div>
            <div className="grupo_form">
              <input onChange={this.handleChange} type="text" className="main_input" id="tel" name="tel" placeholder="Teléfono" value={this.state.tel}></input>
            </div>
            <div className="boton_form_venta">
              <button onClick={this.handleSubmit}>Editar</button>
            </div>
          </form>
        </div>
    )
  }
}

export default EditarUsuarios;