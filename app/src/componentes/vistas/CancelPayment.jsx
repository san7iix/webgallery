import React, {Fragment} from 'react';

class CancelPayment extends React.Component{

    render() {
        return (
            <div class="card_principal">
            <h1>Su pago no fue realizado!</h1>
            <br/>
            <br/>
            <p>Para mas detalles consultar <a href="https://www.sandbox.paypal.com/co/signin">Paypal.</a></p>  
            <br/>
            <br/>
            <h3>Esperamos tenerte de vuelta otra vez!</h3>
            </div>
        );
    }

}

export default CancelPayment;
