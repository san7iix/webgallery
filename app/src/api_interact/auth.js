import config from './config'

class AuthService {
  login(username, password) {
    return fetch(`${config.API_URL}login`,{
      method: 'post',
      body: JSON.stringify({
        "password": password,
        "username": username
      }),
      headers : {
        'Accept': 'application/json',
        'Content-Type' : 'application/json',
      }
    })
    .then(res=>res.json())
    .then(data =>{
      localStorage.setItem(config.tokenName, JSON.stringify(data.token))
      return data
    })
    .catch(() => console.log("Can’t access response. Blocked by browser?"))
  }

  logout() {
    localStorage.removeItem("user_uaeh_token");
  }

  registro(username, email, password,name,surname,tel) {
    return fetch(`${config.API_URL}registro`,{
      method: 'post',
      body: JSON.stringify({
        "username" : username,
        "email" : email,
        "password" : password,
        "name" : name,
        "surname" : surname,
        "tel" : tel
      }),
      headers : {
        'Accept': 'application/json',
        'Content-Type' : 'application/json',
      }
    })
    .then(res=>res.json())
    .then(data =>{
      localStorage.setItem(config.tokenName, JSON.stringify(data.token))
      return data
    })
    .catch(() => console.log("Can’t access response. Blocked by browser?"))
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem(`${config.tokenName}`));
  }
}

export default new AuthService();