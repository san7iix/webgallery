import config from './config'
import AuthService from './auth'


class UsuariosService {
    getUsuarios(){
        return fetch(`${config.API_URL}/usuarios`,{
            method : 'GET',
        })
        .then(res=>res.json())
        .then(data =>{
            return data
        })
        .catch((e) => console.log(e))
    }


    eliminarUsuario(usuario){
        const token = localStorage.getItem(`${config.tokenName}`)
        const myHeader = new Headers({
            'token': token,
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        });
        return fetch(`${config.API_URL}eliminarU/${usuario}`,{
            method: 'DELETE',
            headers : myHeader,
            body: JSON.stringify({
                "username": usuario
            })
        })
        .then(res=>res.json())
        .then(data =>{
            return data
        })
        .catch((e) => console.log(e))        
    }

    obtenerUsuario(usuario){
        return fetch(`${config.API_URL}usuarios/${usuario}`,{
            method : 'GET',
        })
        .then(res=>res.json())
        .then(data =>{
            return data
        })
        .catch((e) => console.log(e))
    }

    editarUsuario(usuario){
        const myHeader = new Headers({
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        });
        return fetch(`${config.API_URL}editarU`,{
            method: 'PUT',
            body: JSON.stringify(usuario),
            headers : {
                'Accept': 'application/json',
                'Content-Type' : 'application/json'
                }
        })
        .then(res=>res.json())
        .then(data =>{
            return data
        })
        .catch((e) => console.log(e))      
    }
}


export default new UsuariosService();