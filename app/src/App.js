import React from 'react';
import logo from './logo.svg';
import './App.css';


import { BrowserRouter as Router , Route, Link, Switch} from "react-router-dom"
import Login from './componentes/vistas/Login'
import Registro from './componentes/vistas/Registro'
import Landing from './componentes/vistas/Landing'
import Vender from './componentes/vistas/Vender'
import Header from './componentes/General/header'
import Footer from './componentes/General/Footer'
import ListarUsuarios from './componentes/vistas/Administrator/Usuario/ListarUsuarios'
import EditarUsuarios from './componentes/vistas/Administrator/Usuario/EditarUsuarios'
import HomeAdmin from './componentes/vistas/Administrator/HomeAdmin'
import Home from './componentes/vistas/Administrator/Home'
import Payment from './componentes/vistas/Payment'
import SuccessPayment from './componentes/vistas/SuccessPayment'
import CancelPayment from './componentes/vistas/CancelPayment'


function App() {
  return (
      <div className="App">
        <Header/>
        <Router>
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route path="/vender" component={Vender} />
            <Route path="/login" component={Login} />
            <Route exact path="/admin" component={HomeAdmin} />
            <Route path="/admin/listarU" component={ListarUsuarios} />
            <Route path="/registro" component={Registro} />
            <Route path="/admin/editarU/:username" component={EditarUsuarios} />
            <Route path="/payment" component={Payment}></Route>
            <Route path="/successpayment" component={SuccessPayment}></Route>
            <Route path="/cancelpayment" component={CancelPayment}></Route> 
          </Switch>
        </Router>
        <Footer/> 
      </div>
  );
}

export default App;
